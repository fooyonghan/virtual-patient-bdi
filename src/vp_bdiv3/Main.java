package vp_bdiv3;

import io.socket.client.Socket;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;

public class Main {
	static Socket socket;

	public static void main(String[] args) {
		PlatformConfiguration config  = PlatformConfiguration.getDefaultNoGui();

		config.addComponent("vp_bdiv3.VirtualPatientBDI.class");
		Starter.createPlatform(config).get();
	}
}