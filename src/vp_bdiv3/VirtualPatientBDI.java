package vp_bdiv3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.JSONObject;

import io.socket.emitter.Emitter;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Body;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Plans;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bridge.component.IExecutionFeature;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.Description;
import patient_communication.SocketComm;
import patient_plans.*;
import patient_goals.*;
import patient_knowledge.ImportantTherapy;
import patient_knowledge.QuestionKnowledge;
import patient_knowledge.TherapyKnowledge;

//--------------------------------- Plans attached to Agent ---------------------------------
@Agent
@Plans({
	@Plan(body = @Body(AskQuestion.class), trigger = @Trigger(goals = GainKnowledge.class)),
	@Plan(body = @Body(AnswerQuestion.class), trigger = @Trigger(goals = ReactToQuestion.class)),
	@Plan(body = @Body(EvaluateSubject.class), trigger = @Trigger(goals = ChangeSubject.class)),
})

@Description("Main Virtual Patient Agent.")
public class VirtualPatientBDI {	
	// --------------------------------- Agent Beliefs ---------------------------------
	public String conversationPurpose;
	
	@Belief
	public void setConversationPurpose(String conversationPurpose) {
		this.conversationPurpose = conversationPurpose;
	}
	
	@Belief
	public String getConversationPurpose() {
		return conversationPurpose;
	}
	
	//	---
	public Map<String, Integer> conversationKnowledge;
	
	@Belief
	public void setConversationKnowledge(Map<String, Integer> conversationKnowledge) {
		this.conversationKnowledge = conversationKnowledge;
	}
	
	@Belief
	public Map<String, Integer> getConversationKnowledge() {
		return conversationKnowledge;
	}
	
	//	---
	public Map<Integer, TherapyKnowledge> therapyKnowledge;
	
	@Belief
	public void setTherapyKnowledge(Map<Integer, TherapyKnowledge> therapyKnowledge) {
		this.therapyKnowledge = therapyKnowledge;
	}
	
	@Belief
	public Map<Integer, TherapyKnowledge> getTherapyKnowledge() {
		return therapyKnowledge;
	}
	
	//	---
	@Belief
	public int therapyOnMind;
	
	@Belief
	public void setTherapyOnMind(int therapyOnMind) {
		this.therapyOnMind = therapyOnMind;
	}
	
	@Belief
	public int getTherapyOnMind() {
		return therapyOnMind;
	}
	
	//	---
	
	@Belief
	public int desireOnMind;
	
	@Belief
	public void setDesireOnMind(int desireOnMind) {
		this.desireOnMind = desireOnMind;
	}
	
	@Belief
	public int getDesireOnMind() {
		return desireOnMind;
	}
	
	//	---
	
	@Belief
	public boolean questionReceived;
	
	@Belief
	public Map<Integer, ImportantTherapy> importantTherapies;

	@Belief
	public Map<Integer, Integer> importantDesires;
	
	@Belief
	public String currentSubject;
	
	@Belief
	public int AWKWARDSILENCE = 14000;
	
	@Belief
	public int MINIMUM_AMOUNT_OF_THERAPIES_TO_MAKE_A_DECISION = 2;
	
	@Belief
	public int MAX_NUMBER_OF_DESIRES;
	
	@Belief
	public boolean isAwkward;
	
	@Belief
	public double desireKnowledgeThreshold;
	
	@Belief
	Timer timer;
	
	// --------------------------------- Agent Logic ---------------------------------
	
	// Agent
	@AgentFeature
	protected IBDIAgentFeature bdiFeature;

	@AgentFeature
	protected IExecutionFeature execFeature;

	@AgentCreated
	public void init() {
		SocketComm.setupServerConnection();
		timer = new Timer();
		
		therapyKnowledge = new HashMap<Integer, TherapyKnowledge>();
		therapyKnowledge.put(1, new TherapyKnowledge());		
		therapyKnowledge.put(2, new TherapyKnowledge());
		
		conversationKnowledge = new HashMap<String, Integer>();
		conversationKnowledge.put("noTherapy", 1);
		conversationKnowledge.put("someTherapy", 2);
		
		setTherapyOnMind(2);
		setDesireOnMind(1);
		
		conversationPurpose = "getKnowledge";
		
		importantTherapies = SocketComm.getImportantTherapies();
		importantDesires = SocketComm.getImportantDesires();

		questionReceived = false;
		isAwkward = false;
		
		desireKnowledgeThreshold = SocketComm.getDesireKnowledgeThreshold();
		System.out.println("DesireKnowledgeThreshold: " + desireKnowledgeThreshold);
		
		MAX_NUMBER_OF_DESIRES = SocketComm.getTotalNumberOfDesires();
		
		System.out.println("Initialisation done.");
	}

	@AgentBody
	public void body() {
		
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
            	updateBeliefs();
				isAwkward = true;
            }
        }, 0, AWKWARDSILENCE);
		
		// Process last received message
		SocketComm.socket.on("newInput", new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				updateBeliefs();
				resetSomeTimer();
			}
		});
		
		// Reset the timer when a question is being typed
		SocketComm.socket.on("typingQuestion", new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				resetSomeTimer();
			}
		});
		
		// Process new question asked
		SocketComm.socket.on("newQuestion", new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				updateBeliefs();
				importantTherapies = SocketComm.getImportantTherapies();
				questionReceived = true;
			}
		});
		
		// Process end of simulation
		// Store feedback results
		SocketComm.socket.on("endConversation", new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				System.out.println("Conversation ended");
				System.out.println("cKnow: " + getConversationKnowledge());
				
				// Current choice
				int therapyId = getCurrentChoice();
				// Choice possible
				boolean choicePossible = choicePossible();
				// SDM followed
				boolean sdmMentioned = checkIfSDM(); //1
				HashMap<Integer, String> prosAndConsMentioned = checkProsAndCons(); //2
				ArrayList<QuestionKnowledge> questions = SocketComm.getQuestions();
				boolean preferencesAsked = preferencesAsked(questions); // 3
				boolean decisionMade = decisionMade(questions); // 4
				// Generated Questions
				JSONObject generatedQuestions = SocketComm.getGeneratedQuestions();
				// Input keywords
				JSONObject inputReceived = SocketComm.getInputReceived();
				
				// Get the values of the perfectSession
				Map<Integer, TherapyKnowledge> perfectSession = new HashMap<Integer, TherapyKnowledge>();
				perfectSession.put(1, new TherapyKnowledge());		
				perfectSession.put(2, new TherapyKnowledge());
				perfectSession.put(3, new TherapyKnowledge());		
				perfectSession.put(4, new TherapyKnowledge());
				SocketComm.getPerfectSession(perfectSession);
				
				Map<Integer, TherapyKnowledge> userSession = new HashMap<Integer, TherapyKnowledge>();
				userSession.put(1, new TherapyKnowledge());		
				userSession.put(2, new TherapyKnowledge());
				userSession.put(3, new TherapyKnowledge());		
				userSession.put(4, new TherapyKnowledge());
				SocketComm.getTherapyKnowledge(userSession);
				
				Map<Integer, TherapyKnowledge> importantDesiresSession = new HashMap<Integer, TherapyKnowledge>();
				importantDesiresSession.put(1, new TherapyKnowledge());		
				importantDesiresSession.put(2, new TherapyKnowledge());
				importantDesiresSession.put(3, new TherapyKnowledge());		
				importantDesiresSession.put(4, new TherapyKnowledge());
				Map<String, Integer> desireSatisfaction = SocketComm.getDesireSatisfaction(importantDesiresSession);
				
				SocketComm.storeFeedback(therapyId, choicePossible, sdmMentioned, prosAndConsMentioned,
						preferencesAsked, decisionMade, questions, generatedQuestions, inputReceived,
						perfectSession, userSession, desireSatisfaction);
			}

			private boolean decisionMade(ArrayList<QuestionKnowledge> questions) {
				for(int i=0; i<questions.size(); i++) {
					QuestionKnowledge q = questions.get(i);
					if(q.getType().equals("knowledge") && (q.getSubject().equals("chemoChoice")) || q.getSubject().equals("hormonesChoice")) {
						return true;
					}
				}
				return false;
			}

			private boolean preferencesAsked(ArrayList<QuestionKnowledge> questions) {
				for(int i=0; i<questions.size(); i++) {
					QuestionKnowledge q = questions.get(i);
					if(q.getType().equals("knowledge") && q.getSubject().equals("desires")) {
						return true;
					}
				}
				return false;
			}

			private HashMap<Integer, String> checkProsAndCons() {
				HashMap<Integer, String> therapySufficient = new HashMap<Integer, String>();
				for(int i=1; i<=4; i++) {
					String isInformed = isSufficientlyInformed(i);
					therapySufficient.put(i, isInformed);
				}
				return therapySufficient;
			}

			private boolean checkIfSDM() {
				return (getConversationKnowledge().containsKey("shareddecisionmaking"));
			}
		});
	}
	
	public void resetSomeTimer() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println("updating timer");
                updateBeliefs();
				isAwkward = true;
            }
        };
        timer.cancel();
        timer = new Timer();
        timer.schedule(timerTask, AWKWARDSILENCE);
    }
	
	// Agent Methods
	public void updateBeliefs() {
		System.out.println("--------START--------");
		
		setTherapyKnowledge(SocketComm.getTherapyKnowledge(therapyKnowledge));
		setConversationKnowledge(SocketComm.updateConversationKnowledge());
		importantTherapies = SocketComm.getImportantTherapies();
		
		evaluateTherapyOnMind();
		evaluateDesireOnMind();

		System.out.println("cKnow: " + getConversationKnowledge());
		System.out.println("Current Therapy On Mind: " + getTherapyOnMind());
		System.out.println("Current Desire On Mind: " + getDesireOnMind());
		
		System.out.println("--------END--------\n\n\n\n\n");
	}
	
	// Set the therapy
	//		if the therapy is relevant and the name is known and the VP has NO knowledge about the therapy,
	//			set the therapy to the current therapy and return
	//		if the therapy is relevant and the name is known and the VP has knowledge about the therapy,
	//			set the therapy to the therapy that has been least talked about
	//		if the therapy name is unknown
	//			set the therapy to the current therapy and end the loop
	public void evaluateTherapyOnMind() {
		for (Integer therapyId : importantTherapies.keySet())
		{
		    ImportantTherapy it = importantTherapies.get(therapyId);
		    TherapyKnowledge t = therapyKnowledge.get(therapyId);
		    TherapyKnowledge tOnMind = therapyKnowledge.get(therapyOnMind);
		    
		    if(getNumberOfKnownTherapies() >= MINIMUM_AMOUNT_OF_THERAPIES_TO_MAKE_A_DECISION) {
		    	if(it.isRelevant() && it.nameKnown()) {
		    		if(t == null) {
			    		setTherapyOnMind(therapyId);
			    		return;
		    		} else if(tOnMind != null) {
				    	if(t.getTherapyFrequency() < tOnMind.getTherapyFrequency()) {
		    				setTherapyOnMind(therapyId);
		    			}
		    		}
		    	}
		    }else if(conversationPurpose == "shareddecisionmaking" && it.isRelevant() && !it.nameKnown()) {
		    	setTherapyOnMind(therapyId);
		    	return;
		    }
		}
	}
	
	// Set the desire:
	// 		if the name of the therapy on mind is unknown,
	//			set the desire on mind to 0 so that the VP will ask about the therapy name
	//		if there is knowledge about the therapy on mind,
	//			set the desire on mind to the highest importance with the least knowledge
	//		if there is no knowledge about the therapy on mind,
	//			set the desire on mind to the highest importance
	//		if therapy on mind and desire on mind are set to MAX_NUMBER_OF_DESIRES+1,
	//			the amount of knowledge for that therapy is satisfied
	public void evaluateDesireOnMind() {
		TherapyKnowledge t = therapyKnowledge.get(therapyOnMind);
		ImportantTherapy it = importantTherapies.get(therapyOnMind);
		
		if(therapyOnMind > MAX_NUMBER_OF_DESIRES && desireOnMind > MAX_NUMBER_OF_DESIRES) {
			return;
		}
		
		if(it.isRelevant()) {
			if(!it.nameKnown()) {
				setDesireOnMind(0);
			} else if(!(therapyKnowledge.get(therapyOnMind) == null)){
				if(therapyKnowledge.get(therapyOnMind).isFullyInformed()) {
					System.out.println("Fully triggered!");
					setTherapyOnMind(MAX_NUMBER_OF_DESIRES+1);
					setDesireOnMind(MAX_NUMBER_OF_DESIRES+1);
				} else {
					int desireId = t.getDesireOnMind();
			    	setDesireOnMind(desireId);
				}
			}else{
				// Set most important desire
				int desireId = 0;
				int maxDesireValue = Integer.MIN_VALUE;
				
				for (Integer key : importantDesires.keySet()) {
				    int importanceToPatient = importantDesires.get(key);
					
					if(importanceToPatient > maxDesireValue) {
						maxDesireValue = importanceToPatient;
						desireId = key;
					}
				}
				setDesireOnMind(desireId);
			}
		}
	}
	
	// Returns the number of therapies which are relevant and the name is known
	public int getNumberOfKnownTherapies() {
		int numberOfTherapiesToChooseFrom = 0;

		for (Integer therapyId : importantTherapies.keySet()) {
			ImportantTherapy it = importantTherapies.get(therapyId);
			if(it.isRelevant() && it.nameKnown()) {		
				numberOfTherapiesToChooseFrom += 1;
			}
		}
		
		return numberOfTherapiesToChooseFrom;
	}
	
	// Return the desires that are most important to the VP
	public String getTopDesires() {
		String topDesires = "";
		for (Integer key : importantDesires.keySet()) {
		    int importanceToPatient = importantDesires.get(key);
			
			if(importanceToPatient > desireKnowledgeThreshold) {
				topDesires += ("-" + key); 
			}
		}
		return topDesires;
	}
	
	// Check if a given therapy has been discussed enough
	public String isSufficientlyInformed(int therapyId) {
		ImportantTherapy it = importantTherapies.get(therapyId);
		TherapyKnowledge t = therapyKnowledge.get(therapyId);
		
		if(it.isRelevant() && it.nameKnown()){
			if (t != null && t.isSufficientlyInformed(importantDesires, desireKnowledgeThreshold)) {
				System.out.println("Genoeg kennis!");
				return "sufficient";
			} else {
				System.out.println("Nog niet genoeg kennis!");
				return "insufficient";
			}
		} else {
			return "unknownTherapy";
		}
	}

	// Check whether the VP has enough knowledge on each important therapy
	// A choice can be made if there are at least two therapies with
	//		isRelevant, nameKnown and therapyKnowledge not null
	public boolean choicePossible() {
		int numberOfTherapiesToChooseFrom = 0;
		
		for (Integer therapyId : importantTherapies.keySet()) {
			ImportantTherapy it = importantTherapies.get(therapyId);
			if(it.isRelevant() && it.nameKnown() && (therapyKnowledge.get(therapyId) != null)) {
				if(therapyKnowledge.get(therapyId).isSufficientlyInformed(importantDesires, desireKnowledgeThreshold)) {					
					numberOfTherapiesToChooseFrom += 1;
				}
			}
		}
		
		// desireKnowledgeThreshold
		return (numberOfTherapiesToChooseFrom >= MINIMUM_AMOUNT_OF_THERAPIES_TO_MAKE_A_DECISION);
	}

	// Return what the current choice would be based on a utility function
	//		Return 0 if the VP needs more information
	//		Return 1, 2, 3, 4 which represents the therapyId that has the highest preference
	public int getCurrentChoice() {
		System.out.println("What should we choose?");
		int preferredTherapy = 0;
		double highestImpact = -Double.MAX_VALUE;
		
		if(!choicePossible()) {
			return preferredTherapy;
		} else {
			for (Integer therapyId : importantTherapies.keySet()) {
				TherapyKnowledge t = therapyKnowledge.get(therapyId);
				ImportantTherapy it = importantTherapies.get(therapyId);
				
				if(it.isRelevant() && it.nameKnown() && t != null) {
					double impact = t.getTherapyImpact();
	
					if(impact > highestImpact) {
						preferredTherapy = therapyId;
					}
	
					System.out.println(impact);
					System.out.println("preferred: " + preferredTherapy);
				}
			}
			return preferredTherapy;
		}
	}
}
