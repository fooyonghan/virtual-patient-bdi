package patient_knowledge;

import java.util.HashMap;
import java.util.Map;

import patient_communication.SocketComm;

public class TherapyKnowledge {
	private static int MINIMUM_AMOUNT_OF_KNOWLEDGE = 1;

	HashMap<Integer, DesireKnowledge> desires;
	boolean isRelevant;
	boolean nameKnown;

	public TherapyKnowledge() {
		desires = new HashMap<Integer, DesireKnowledge>();
	}

	public void printDesires() {
		for (Integer key : desires.keySet()) {
			DesireKnowledge d = desires.get(key);
			System.out.println("did: " + key);
			System.out.println("frequencySum: " + d.getSumFrequency());
		}
	}

	public DesireKnowledge getDesire(int desireId) {
		if (desires.size() == 0) {
			return null;
		} else {
			DesireKnowledge d = desires.get(desireId);
			return d;
		}
	}

	public boolean therapyExists() {
		if (desires.size() == 0 || desires.equals(null)) {
			return false;
		} else {
			return true;
		}
	}

	public void setRelevant(boolean isRelevant) {
		this.isRelevant = isRelevant;
	}

	public void setNameKnown(boolean nameKnown) {
		this.nameKnown = nameKnown;
	}

	public boolean isRelevant() {
		return isRelevant;
	}

	public boolean nameKnown() {
		return nameKnown;
	}

	// Returns the desire id that has the highest importance and has not been
	// discussed enough.
	public int getDesireOnMind() {
		int desireId = 0;
		Map<Integer, Integer> importantDesires = SocketComm.getImportantDesires();

		int maxDesireValue = Integer.MIN_VALUE;

		for (Integer key : importantDesires.keySet()) {
			int importanceToPatient = importantDesires.get(key);

			// Get highest desireID
			if (importanceToPatient > maxDesireValue) {
				maxDesireValue = importanceToPatient;
				desireId = key;

				// If the desire has not been talked about enough, set that as
				// desireOnMind
				DesireKnowledge d = desires.get(desireId);
				if (d != null) {
					System.out.println("Existing D!");
					if (!(d.frequencySum >= MINIMUM_AMOUNT_OF_KNOWLEDGE)) {
						return desireId;
					}
				}

				// If the desire has not been talked about at all, set taht as
				// desireOnMind
				if (!desires.containsKey(desireId)) {
					return desireId;
				} else {
					maxDesireValue = Integer.MIN_VALUE;
				}
			}
		}
		return desireId;
	}

	public int getLowestTherapyKnowledge() {
		int desireId = 0;
		int minValue = Integer.MAX_VALUE;

		for (Integer key : desires.keySet()) {
			DesireKnowledge d = desires.get(key);
			int sumFrequency = d.getSumFrequency();

			if (sumFrequency < minValue) {
				minValue = sumFrequency;
				desireId = key;
			}
		}

		return desireId;
	}

	public int getTherapyFrequency() {
		int therapyFrequency = 0;

		for (Integer key : desires.keySet()) {
			DesireKnowledge d = desires.get(key);
			int desireSumFrequency = d.getSumFrequency();

			therapyFrequency += desireSumFrequency;
		}

		return therapyFrequency;
	}

	public double getTherapyImpact() {
		double therapyImpact = 0;

		for (Integer key : desires.keySet()) {
			DesireKnowledge d = desires.get(key);
			double impactSum = d.getImpactSum();
			double importanceToPatient = d.getImportanceToPatient();

			therapyImpact += (impactSum * importanceToPatient);
		}

		return therapyImpact / desires.size();
	}

	public boolean isSufficientlyInformed(Map<Integer, Integer> importantDesires, double desireKnowledgeThreshold) {
		for (Integer key : importantDesires.keySet()) {
			int importanceToPatient = importantDesires.get(key);

			if (importanceToPatient > desireKnowledgeThreshold) {
				System.out.println("key: " + key);
				DesireKnowledge d = desires.get(key);
				if (d == null) {
					System.out.println("Ik besta niet");
					return false;
				} else {
					System.out.println("freqSum: " + d.frequencySum);

					if (!(d.frequencySum >= MINIMUM_AMOUNT_OF_KNOWLEDGE)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public boolean isFullyInformed() {
		int totalNumberOfDesires = SocketComm.getTotalNumberOfDesires();

		return (desires.size() == totalNumberOfDesires);
	}

	public void updateDesire(int desireId, DesireKnowledge dA) {
		desires.put(desireId, dA);
	}
}
