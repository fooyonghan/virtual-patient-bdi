package patient_knowledge;

public class ImportantTherapy {
	boolean isRelevant;
	boolean nameKnown;
	
	public ImportantTherapy(boolean isRelevant, boolean nameKnown) {
		this.isRelevant = isRelevant;
		this.nameKnown = nameKnown;
	}
	
	public boolean isRelevant() {
		return isRelevant;
	}
	
	public boolean nameKnown() {
		return nameKnown;
	}
}
