package patient_knowledge;

public class ConversationKnowledge {
	String subjectName;
	int count;
	
	ConversationKnowledge(String subjectName, int count) {
		this.subjectName = subjectName;
		this.count = count;
	}
	
	String getSubjectName() {
		return subjectName;
	}
	
	int getCount() {
		return count;
	}
	
	void setCount(int count) {
		this.count = count;
	}
}
