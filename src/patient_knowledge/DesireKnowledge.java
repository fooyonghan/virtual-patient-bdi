package patient_knowledge;

import java.util.HashMap;
import java.util.Map.Entry;

public class DesireKnowledge {
	int frequencySum;
	int impactSum;
	int importanceToPatient;
	HashMap<Integer, EffectKnowledge> effects;
	
	public DesireKnowledge(int frequencySum, int impactSum, int importanceToPatient) {
		this.frequencySum = frequencySum;
		this.impactSum = impactSum;
		this.importanceToPatient = importanceToPatient;
		effects = new HashMap<Integer, EffectKnowledge>();
	}
	
	public void updateEffect(int effectId, int frequency, int impact) {
		EffectKnowledge e = new EffectKnowledge(frequency, impact);
		effects.put(effectId, e);
	}
	
	public int getImportanceToPatient() {
		return importanceToPatient;
	}
	
	public void setImpactSum() {
		impactSum = 0;
		
		for (Entry<Integer, EffectKnowledge> entry : effects.entrySet()) {
		    EffectKnowledge eA = entry.getValue();
		    impactSum += eA.impact;
		}
	}
	
	public int getImpactSum() {
		return impactSum;
	}
	
	public void setSumFrequency() {
		frequencySum = 0;
		
		for (Entry<Integer, EffectKnowledge> entry : effects.entrySet()) {
		    EffectKnowledge eA = entry.getValue();
//		    eA.printAttributes();
		    frequencySum += eA.frequency;		    
		}
	}
	
	public int getSumFrequency() {
		return frequencySum;
	}
}