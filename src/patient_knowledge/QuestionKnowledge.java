package patient_knowledge;

public class QuestionKnowledge {
	String question;
	String questionType;
	String questionSubject;
	
	public QuestionKnowledge(String question, String questionType, String questionSubject) {
		this.question = question;
		this.questionType = questionType;
		this.questionSubject = questionSubject;
	}
	
	public String getType() {
		return questionType;
	}
	
	public String getSubject() {
		return questionSubject;
	}
	
	public String getQuestion() {
		return question;
	}

	public void printQuestion() {
		System.out.println("TESTING QUESTION: " + question);
	}
}
