package patient_knowledge;

public class EffectKnowledge {
	int frequency;
	int impact;
	
	public EffectKnowledge(int frequency, int impact) {
		this.frequency = frequency;
		this.impact = impact;
	}
	
	public void printAttributes() {
		System.out.println("frequency: " + frequency);
		System.out.println("impact: " + impact);
	}
}
