package patient_communication;

import java.net.URISyntaxException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;
import io.socket.client.IO;
import io.socket.client.Socket;
import patient_knowledge.DesireKnowledge;
import patient_knowledge.ImportantTherapy;
import patient_knowledge.QuestionKnowledge;
import patient_knowledge.TherapyKnowledge;

public class SocketComm {
	public static Socket socket;
	public static String url = "jdbc:mysql://127.0.0.1:3306/virtual_patient";
	public static String username = "root";
	public static String password = "cthulhuCAT?33";

	/*
	 * Setup a connection to the local Flask server.
	 */
	public static void setupServerConnection() {
		try {
			System.out.println("Connecting to server...");

			socket = IO.socket("http://127.0.0.1:5000");
			socket.connect();

			System.out.println("Connected to server!");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	// Check if anything has been said
	public static boolean nothingHasBeenSaid() {
		int count = 0;

		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT COUNT(*) AS count FROM virtual_patient.input";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				count = rs.getInt("count");
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}

		return count==0;
	}
	
	// Update therapy knowledge
	public static Map<Integer, TherapyKnowledge> getTherapyKnowledge(Map<Integer, TherapyKnowledge> therapyKnowledge) {		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT virtual_patient.effect_on_therapy.therapyId," +
					"virtual_patient.effect_on_therapy.frequency," +
					"virtual_patient.effect_on_therapy.impact," +
					"virtual_patient.effect_on_desire.effectId," +
					"virtual_patient.effect_on_desire.desireId," +
					"virtual_patient.desire.importanceToPatient, " +
					"virtual_patient.therapy.isRelevant, " +
			        "virtual_patient.therapy.nameKnown " +
					"FROM virtual_patient.effect_on_desire " +
					"JOIN virtual_patient.effect_on_therapy " +
					"ON virtual_patient.effect_on_desire.effectId = virtual_patient.effect_on_therapy.effectId " +
					"JOIN virtual_patient.therapy " +
					"ON virtual_patient.effect_on_therapy.therapyId = virtual_patient.therapy.id " +
					"JOIN virtual_patient.desire " +
					"ON virtual_patient.desire.id = virtual_patient.effect_on_desire.desireId " +
					"WHERE frequency<>0";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while (rs.next())
			{	
				int therapyId = Integer.parseInt(rs.getString("therapyId"));
				int effectId = Integer.parseInt(rs.getString("effectId"));
				int desireId = Integer.parseInt(rs.getString("desireId"));
				int frequency = Integer.parseInt(rs.getString("frequency"));
				int impact = Integer.parseInt(rs.getString("impact"));
				int importanceToPatient = Integer.parseInt(rs.getString("importanceToPatient"));
				boolean isRelevant = rs.getBoolean("isRelevant");
				boolean nameKnown = rs.getBoolean("nameKnown"); 
								
				TherapyKnowledge t = therapyKnowledge.get(therapyId);
				// Does this therapy exist?
				if (t != null) {
					DesireKnowledge d = t.getDesire(desireId);
					// Does this desire exist in this therapy?
					if (d != null) {
						d.updateEffect(effectId, frequency, impact);
					} else {
						d = new DesireKnowledge(frequency,impact,importanceToPatient);
						d.updateEffect(effectId, frequency, impact);
						t.updateDesire(desireId, d);
					}
					d.setSumFrequency();
					d.setImpactSum();
					
					t.setRelevant(isRelevant);
					t.setNameKnown(nameKnown);
					
					therapyKnowledge.put(therapyId, t);
				} else {
					DesireKnowledge d = new DesireKnowledge(frequency, impact, importanceToPatient);
					d.updateEffect(effectId, frequency, impact);
					t = new TherapyKnowledge();
					t.updateDesire(desireId, d);
					therapyKnowledge.put(therapyId, t);
				}
			}
			st.close();
			return therapyKnowledge;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}
	
	/*
	 * Update relevant therapies to SDM mode 
	 */
	public static void updateRelevantTherapies() {
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "UPDATE virtual_patient.therapy t2 JOIN virtual_patient.therapy t3 JOIN virtual_patient.therapy t4 " +
				    "ON t2.id = 2 AND t3.id = 3 AND t4.id = 4 " +
				    "SET t2.isRelevant = 0, t3.isRelevant = 1, t4.isRelevant = 1";

			Statement st = (Statement) connection.createStatement();
			st.executeUpdate(query);						
			st.close();
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}
	
	/*
	 * Update nameKnown of important therapies
	 */
	public static void updateTherapiesNameKnown(int therapyId) {
		// Update importantTherapy nameKnown where importantTherapy.name equals therapyName
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "UPDATE virtual_patient.therapy " +
					"SET virtual_patient.therapy.nameKnown = 1 " +
					"WHERE virtual_patient.therapy.id =" + therapyId;

			Statement st = (Statement) connection.createStatement();
			st.executeUpdate(query);						
			st.close();
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}

	/*
	 * Retrieve the history of subjects from the database and return the amount of times the corresponding subject
	 * has been discussed
	 */
	public static Map<String, Integer> updateConversationKnowledge() {
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT virtual_patient.history.subject, COUNT(*) as count " +
					"FROM virtual_patient.history " +
					"GROUP BY virtual_patient.history.subject " +
					"ORDER BY count DESC";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			Map<String, Integer> result = new HashMap<String, Integer>();
			
			while (rs.next())
			{
				String subject = rs.getString("subject");
				int count = Integer.parseInt((rs.getString("count")));
				
				result.put(subject, count);				
			}
						
			st.close();
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}
	
	/*
	 * Set the subject in the database
	 */
	public static void setSubjectInDb(int therapyId) {
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "INSERT INTO virtual_patient.history (subject) " +
					"SELECT DISTINCT virtual_patient.history.subject " +
					"FROM virtual_patient.history " +
					"JOIN virtual_patient.therapy " +
					"ON virtual_patient.therapy.therapyName = virtual_patient.history.subject " +
					"WHERE virtual_patient.therapy.id = " + therapyId;

			Statement st = (Statement) connection.createStatement();
			st.executeUpdate(query);						
			st.close();
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}

	/*
	 * Retrieve the keywords from the last commited database entry.
	 */
	public static String getKeywords() {
		String keywords = "";

		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT * FROM input ORDER BY id DESC LIMIT 1";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				keywords = rs.getString("keywords");
				return keywords;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}

		return keywords;
	}

	/*
	 * Retrieve the full plain text message from the database.
	 */
	public static String getVanillaMessage() {
		String message = "";

		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT * FROM input ORDER BY id DESC LIMIT 1";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				message = rs.getString("message");
				return message;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}

		return message;
	}
	
	/*
	 * Retrieve therapies that are currently important to the virtual patient.
	 */
	public static Map<Integer, ImportantTherapy> getImportantTherapies() {
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT	virtual_patient.therapy.id, " +
					"virtual_patient.therapy.isRelevant, " +
					"virtual_patient.therapy.nameKnown " +
					"FROM virtual_patient.therapy";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			Map<Integer, ImportantTherapy> result = new LinkedHashMap<Integer, ImportantTherapy>();
			
			while (rs.next())
			{
				int therapyId = Integer.parseInt(rs.getString("id"));
				boolean isRelevant = rs.getBoolean("isRelevant");
				boolean nameKnown = rs.getBoolean("nameKnown");
				
				ImportantTherapy it = new ImportantTherapy(isRelevant, nameKnown);
				result.put(therapyId, it);
			}
						
			st.close();
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}

	
	/*
	 * Retrieve all the virtual patient's desires and their corresponding importance.
	 */
	public static Map<Integer, Integer> getImportantDesires() {
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT	virtual_patient.desire.id, " +
					"virtual_patient.desire.importanceToPatient " +
					"FROM virtual_patient.desire " +
					"ORDER BY importanceToPatient DESC";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			Map<Integer, Integer> result = new LinkedHashMap<Integer, Integer>();
			
			while (rs.next())
			{
				int desireId = Integer.parseInt(rs.getString("id"));
				int importanceToPatient = Integer.parseInt(rs.getString("importanceToPatient"));
				
				result.put(desireId, importanceToPatient);				
			}
						
			st.close();
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}
	
	// Get the latest question asked
	public static QuestionKnowledge getCurrentQuestion() {
		QuestionKnowledge result = null;
		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT * FROM question ORDER BY id DESC LIMIT 1";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				String question = rs.getString("question");
				String questionType = rs.getString("questionType");
				String questionSubject = rs.getString("questionSubject");
				
				result = new QuestionKnowledge(question, questionType, questionSubject);
				
				return result;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
		return result;
	}
	
	// Get all the questions asked
	public static ArrayList<QuestionKnowledge> getQuestions() {
		ArrayList<QuestionKnowledge> result = new ArrayList<QuestionKnowledge>();
		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT * FROM question";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				String question = rs.getString("question");
				String questionType = rs.getString("questionType");
				String questionSubject = rs.getString("questionSubject");
				
				QuestionKnowledge q = new QuestionKnowledge(question, questionType, questionSubject);	
				result.add(q);
			}
			st.close();
			
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
		return result;
	}
	
	/*
	 * Calculate desireKnowledgeThreshold. This represents the minimum amount of desires the virtual patient
	 * would want to know about before a decision is possible
	 */
	public static double getDesireKnowledgeThreshold() {
		double desireKnowledgeThreshold = 0;

		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT AVG(virtual_patient.desire.importanceToPatient) AS avg "
							+ "FROM virtual_patient.desire";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				desireKnowledgeThreshold = rs.getDouble("avg");
				return desireKnowledgeThreshold;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}

		return desireKnowledgeThreshold;
	}
	
	/*
	 * Get total number of desires
	 */
	public static int getTotalNumberOfDesires() {
		int totalNumberOfDesires = 0;

		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT COUNT(*) AS count FROM virtual_patient.desire";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next())
			{
				totalNumberOfDesires = rs.getInt("count");
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}

		return totalNumberOfDesires;
	}

	/*
	 * Send a bdi-based message to the local Flask server.
	 */
	public static void sendMessage(String msg) {
		socket.emit("speakMessage", msg);
	}
	
	/*
	 * Send a bdi-based message to the local Flask server.
	 */
//	public static void askQuestion(String msg) {
//		socket.emit("askQuestion", msg);
//	}

	/*
	 * Send a api.ai message to the local Flask server.
	 */
	public static void chitchat(String msg) {
		socket.emit("chitchat", msg);
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject getGeneratedQuestions() {
		JSONObject obj = new JSONObject();
		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT * FROM generated_question";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			int index = 0;
			while (rs.next())
			{
				JSONObject questionData = new JSONObject();
				
				String generatedQuestion = rs.getString("generatedQuestion");
				String askedTid = rs.getString("askedTid");
				String askedDid = rs.getString("askedDid");
				String generatedQuestionType = rs.getString("generatedQuestionType");
				
				questionData.put("generatedQuestion" , generatedQuestion);
				questionData.put("askedTid" , askedTid);
				questionData.put("askedDid" , askedDid);
				questionData.put("generatedQuestionType" , generatedQuestionType);
				
				obj.put(index, questionData);
				
				index++;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
		
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject getInputReceived() {
		JSONObject obj = new JSONObject();
		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = 	"SELECT * FROM input";
			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			int index = 0;
			while (rs.next())
			{
				JSONObject inputData = new JSONObject();
				
				String message = rs.getString("message");
				String keywords = rs.getString("keywords");
				
				inputData.put("message", message);
				inputData.put("keywords", keywords);
				
				obj.put(index, inputData);
				
				index++;
			}
			st.close();

		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
		
		return obj;
	}
	
	// Store feedback
	@SuppressWarnings("unchecked")
	public static void storeFeedback(int preferredTherapy, boolean choicePossible, boolean sdmMentioned,
		HashMap<Integer, String> prosAndConsMentioned, boolean preferencesAsked, boolean decisionMade,
		ArrayList<QuestionKnowledge> questions, JSONObject generatedQuestions, JSONObject inputReceived,
		Map<Integer, TherapyKnowledge> perfectSession, Map<Integer, TherapyKnowledge> userSession,
		Map<String, Integer> desireSatisfaction) {
		for(int i=0; i<questions.size(); i++) {
			System.out.println(questions.get(i).getQuestion());
		}
		
		JSONObject obj = new JSONObject();
		obj.put("preferredTherapy", preferredTherapy);
		obj.put("choicePossible", choicePossible);
		obj.put("sdmMentioned", sdmMentioned);
		obj.put("prosAndConsMentioned", prosAndConsMentioned);
		obj.put("preferencesAsked", preferencesAsked);
		obj.put("decisionMade", decisionMade);
		
		JSONObject questionsAsked = new JSONObject();
		for(int i=0; i<questions.size(); i++) {
			JSONObject q = new JSONObject();
			String question = questions.get(i).getQuestion();
			String questionSubject = questions.get(i).getSubject();
			String questionType = questions.get(i).getType();
			q.put("question", question);
			q.put("questionSubject", questionSubject);
			q.put("questionType", questionType);
			questionsAsked.put(i, q);
		}
		obj.put("questionsAsked", questionsAsked);
		obj.put("generatedQuestions", generatedQuestions);
		obj.put("inputReceived", inputReceived);
		
		JSONObject perfectTherapyResults = new JSONObject(); 
		for(int i=1; i<perfectSession.size()+1; i++) {
			JSONObject perfectTherapyResult = new JSONObject();
			TherapyKnowledge t = perfectSession.get(i);
			double impact = t.getTherapyImpact();
			System.out.println(impact);
			
			perfectTherapyResult.put("therapyId", i);
			perfectTherapyResult.put("therapyImpact", impact);
			perfectTherapyResults.put(i-1, perfectTherapyResult);
		}
		obj.put("perfectTherapyResults", perfectTherapyResults);
		
		JSONObject userTherapyResults = new JSONObject(); 
		for(int i=1; i<userSession.size()+1; i++) {
			JSONObject userTherapyResult = new JSONObject();
			TherapyKnowledge t = userSession.get(i);
			double impact = t.getTherapyImpact();
			System.out.println(impact);
			
			userTherapyResult.put("therapyId", i);
			userTherapyResult.put("therapyImpact", impact);
			userTherapyResults.put(i-1, userTherapyResult);
		}
		obj.put("userTherapyResults", userTherapyResults);
		
		// Vergelijk in hoeverre importance to patient desires is bereikt
		JSONObject desireSatisfactionResults = new JSONObject();
		for ( String key : desireSatisfaction.keySet() ) {
		    desireSatisfactionResults.put(key, desireSatisfaction.get(key));
		}
		obj.put("desireSatisfactionResults", desireSatisfactionResults);
		
		try (FileWriter file = new FileWriter("/Users/fooyonghan/Developer/virtual-patient/app/results/rawFeedback.txt")) {
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Get how far desires have been met
	public static Map<String, Integer> getDesireSatisfaction(Map<Integer, TherapyKnowledge> therapyKnowledge) {		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT virtual_patient.effect_on_therapy.therapyId," +
					"virtual_patient.effect_on_therapy.frequency," +
					"virtual_patient.effect_on_therapy.impact," +
					"virtual_patient.effect_on_desire.effectId," +
					"virtual_patient.effect_on_desire.desireId," +
					"virtual_patient.desire.importanceToPatient, " +
					"virtual_patient.therapy.isRelevant, " +
			        "virtual_patient.therapy.nameKnown " +
					"FROM virtual_patient.effect_on_desire " +
					"JOIN virtual_patient.effect_on_therapy " +
					"ON virtual_patient.effect_on_desire.effectId = virtual_patient.effect_on_therapy.effectId " +
					"JOIN virtual_patient.therapy " +
					"ON virtual_patient.effect_on_therapy.therapyId = virtual_patient.therapy.id " +
					"JOIN virtual_patient.desire " +
					"ON virtual_patient.desire.id = virtual_patient.effect_on_desire.desireId";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			Map<String, Integer> result = new HashMap<String, Integer>();
			
			while (rs.next())
			{	
				String therapyId = rs.getString("therapyId");
				String effectId = rs.getString("effectId");
				String desireId = rs.getString("desireId");
				String importanceToPatient = rs.getString("importanceToPatient");
				int frequency = Integer.parseInt(rs.getString("frequency"));
				
				String key  = therapyId + "-" + desireId + "-" + effectId + "-" + importanceToPatient;
				result.put(key, frequency);
			}
			st.close();
			return result;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}

	// Create the perfect conversation outcome
	public static Map<Integer, TherapyKnowledge> getPerfectSession(Map<Integer, TherapyKnowledge> therapyKnowledge) {		
		try (Connection connection = (Connection) DriverManager.getConnection(url, username, password)) {
			String query = "SELECT virtual_patient.effect_on_therapy.therapyId," +
					"virtual_patient.effect_on_therapy.frequency," +
					"virtual_patient.effect_on_therapy.impact," +
					"virtual_patient.effect_on_desire.effectId," +
					"virtual_patient.effect_on_desire.desireId," +
					"virtual_patient.desire.importanceToPatient, " +
					"virtual_patient.therapy.isRelevant, " +
			        "virtual_patient.therapy.nameKnown " +
					"FROM virtual_patient.effect_on_desire " +
					"JOIN virtual_patient.effect_on_therapy " +
					"ON virtual_patient.effect_on_desire.effectId = virtual_patient.effect_on_therapy.effectId " +
					"JOIN virtual_patient.therapy " +
					"ON virtual_patient.effect_on_therapy.therapyId = virtual_patient.therapy.id " +
					"JOIN virtual_patient.desire " +
					"ON virtual_patient.desire.id = virtual_patient.effect_on_desire.desireId";

			Statement st = (Statement) connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			while (rs.next())
			{	
				int therapyId = Integer.parseInt(rs.getString("therapyId"));
				int effectId = Integer.parseInt(rs.getString("effectId"));
				int desireId = Integer.parseInt(rs.getString("desireId"));
				int frequency = Integer.parseInt(rs.getString("frequency"));
				int impact = Integer.parseInt(rs.getString("impact"));
				int importanceToPatient = Integer.parseInt(rs.getString("importanceToPatient"));
				boolean isRelevant = rs.getBoolean("isRelevant");
				boolean nameKnown = rs.getBoolean("nameKnown"); 
								
				TherapyKnowledge t = therapyKnowledge.get(therapyId);
				// Does this therapy exist?
				if (t != null) {
					DesireKnowledge d = t.getDesire(desireId);
					// Does this desire exist in this therapy?
					if (d != null) {
						d.updateEffect(effectId, frequency, impact);
					} else {
						d = new DesireKnowledge(frequency,impact,importanceToPatient);
						d.updateEffect(effectId, frequency, impact);
						t.updateDesire(desireId, d);
					}
					d.setSumFrequency();
					d.setImpactSum();
					
					t.setRelevant(isRelevant);
					t.setNameKnown(nameKnown);
					
					therapyKnowledge.put(therapyId, t);
				} else {
					DesireKnowledge d = new DesireKnowledge(frequency, impact, importanceToPatient);
					d.updateEffect(effectId, frequency, impact);
					t = new TherapyKnowledge();
					t.updateDesire(desireId, d);
					therapyKnowledge.put(therapyId, t);
				}
			}
			st.close();
			return therapyKnowledge;
		} catch (SQLException e) {
			throw new IllegalStateException("Cannot connect the database!", e);
		}
	}
}
