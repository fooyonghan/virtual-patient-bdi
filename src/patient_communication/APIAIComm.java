package patient_communication;

import ai.api.*;
import ai.api.model.*;

public class APIAIComm {
	public static String getResponse(String msg) {
		AIConfiguration configuration = new AIConfiguration("4f45e68fdfcf44b58d201cec4d6ed518");
		AIDataService dataService = new AIDataService(configuration);

		try {
			AIRequest request = new AIRequest(msg);
			AIResponse response = dataService.request(request);

			if (response.getStatus().getCode() == 200) {
//				System.out.println(response.getResult().getFulfillment().getSpeech());
				return response.getResult().getFulfillment().getSpeech();
			} else {
				System.err.println(response.getStatus().getErrorDetails());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
