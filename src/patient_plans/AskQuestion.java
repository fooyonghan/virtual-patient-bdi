package patient_plans;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import patient_communication.SocketComm;
import vp_bdiv3.VirtualPatientBDI;

@Plan
public class AskQuestion
{
	@PlanCapability
	protected VirtualPatientBDI vp;
	
	@PlanBody
	private void enoughKnowledge() {
		// Bij het stellen van een vraag en de therapie switched
		// moet er dan automatisch geswitched worden naar de therapie waar ze iets van wil weten? JA!
		
		if(!SocketComm.nothingHasBeenSaid()) {
			SocketComm.sendMessage("question-" + vp.getDesireOnMind() + "," + vp.getTherapyOnMind());
			SocketComm.setSubjectInDb(vp.getTherapyOnMind());
		}
		vp.isAwkward = false;
	}
}