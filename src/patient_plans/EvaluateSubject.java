package patient_plans;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import patient_communication.SocketComm;
import vp_bdiv3.VirtualPatientBDI;

@Plan
public class EvaluateSubject
{	
	// Wordt pas iets mee gedaan in de ronde na de update
	//	private static int KNOWLEDGE_BOUND = 1;

	@PlanCapability
	protected VirtualPatientBDI vp;
	
	@PlanBody
	public void evaluateSubject() {
//		System.out.println("sdmKnow: " + vp.getConversationKnowledge().get("shareddecisionmaking"));
//		System.out.println("hormoneKnow: " + vp.getConversationKnowledge().get("hormonetherapy"));
//		System.out.println("chemoKnow: " + vp.getConversationKnowledge().get("chemotherapy"));
		
		Integer value = vp.conversationKnowledge.get("shareddecisionmaking");
		if (value != null) {
			if(vp.conversationKnowledge.get("shareddecisionmaking") >= 1) {
				vp.setConversationPurpose("shareddecisionmaking");			
				SocketComm.updateRelevantTherapies();
			}
		}
		
		value = vp.conversationKnowledge.get("hormonetherapy");
		if (value != null) {
			if(vp.getConversationKnowledge().get("hormonetherapy") >= 1) {	
				SocketComm.updateTherapiesNameKnown(3);
			}
		}
		
		value = vp.conversationKnowledge.get("chemotherapy");
		if (value != null) {
			if(vp.getConversationKnowledge().get("chemotherapy") >= 1) {	
				SocketComm.updateTherapiesNameKnown(4);
			}
		}
		
		// If history has a therapy after "shareddecisionmaking" set nameKnown to true in importantTherapy
		// Also set desireOnMind to desire with highest importance
		// Keep thinking about setting desireOnMind and therapyOnMind
		
		// Bugs:
		//		KnowledgeBound works after 3 times?!
		//		Chemo works after hormones has been talked about?!
		
	}
}