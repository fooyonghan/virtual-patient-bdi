package patient_plans;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import patient_communication.SocketComm;
import patient_knowledge.QuestionKnowledge;
import vp_bdiv3.VirtualPatientBDI;

@Plan
public class AnswerQuestion
{	
	@PlanCapability
	protected VirtualPatientBDI vp;

	@PlanBody
	private void wantsToAnswer() {
		QuestionKnowledge question = SocketComm.getCurrentQuestion();

		if(question != null) {
			getAnswer(question);
		}

		vp.questionReceived = false;
	}

	// Logic for evaluating what type of question is being asked
	// Several methods for returning the corresponding answer
	public void getAnswer(QuestionKnowledge question) {
		System.out.println("Q subject: " + question.getSubject());
		System.out.println("Q type: " + question.getType());
		System.out.println("Q question: " + question.getQuestion());

		switch (question.getSubject()) {
		case "unknown":
			SocketComm.sendMessage("answer-" + "unknown" + "," + "unknown" + ";" + vp.desireOnMind);
			break;
		case "choicePossible":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "choicePossible" + ";" + vp.choicePossible());
			break;
		case "preference":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "therapyPreference" + ";" + vp.getCurrentChoice());
			break;
		case "desires":
			String topDesires = vp.getTopDesires();
			String[] topDesiresArray = topDesires.split("-");
			for (String topDesire: topDesiresArray) {
			    SocketComm.sendMessage("answer-" + "knowledge" + "," + "desires" + ";" + topDesire);
			}
			break;
		case "desireSatisfaction":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "desireSatisfaction" + ";" + vp.desireOnMind + "+" + vp.therapyOnMind);
			break;
		case "conversationPurpose":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "conversationPurpose" + ";" + vp.conversationPurpose);
			break;
		case "chemo":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "chemo" + ";" + vp.isSufficientlyInformed(4));
			break;
		case "hormones":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "hormones" + ";" + vp.isSufficientlyInformed(3));
			break;
		case "chemoChoice":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "chemoChoice" + ";" + vp.isSufficientlyInformed(4));
			break;
		case "hormonesChoice":
			SocketComm.sendMessage("answer-" + "knowledge" + "," + "hormonesChoice" + ";" + vp.isSufficientlyInformed(3));
			break;
		default:
			break;
		}
	}
//	
//	// Return the desires that are most important to the VP
//	private String getTopDesires() {
//		String topDesires = "";
//		for (Integer key : vp.importantDesires.keySet()) {
//		    int importanceToPatient = vp.importantDesires.get(key);
//			
//			if(importanceToPatient > vp.desireKnowledgeThreshold) {
//				topDesires += ("-" + key); 
//			}
//		}
//		return topDesires;
//	}
//	
//	// Check if a given therapy has been discussed enough
//	private String isSufficientlyInformed(int therapyId) {
//		ImportantTherapy it = vp.importantTherapies.get(therapyId);
//		TherapyKnowledge t = vp.therapyKnowledge.get(therapyId);
//		
//		if(it.isRelevant() && it.nameKnown()){
//			if (t != null && t.isSufficientlyInformed()) {
//				return "sufficient";
//			} else {
//				return "insufficient";
//			}
//		} else {
//			return "unknownTherapy";
//		}
//	}
//
//	// Check whether the VP has enough knowledge on each important therapy
//	// A choice can be made if there are at least two therapies with
//	//		isRelevant, nameKnown and therapyKnowledge not null
//	private boolean choicePossible() {
//		int numberOfTherapiesToChooseFrom = 0;
//		
//		for (Integer therapyId : vp.importantTherapies.keySet()) {
//			ImportantTherapy it = vp.importantTherapies.get(therapyId);
//			if(it.isRelevant() && it.nameKnown() && (vp.therapyKnowledge.get(therapyId) != null)) {
//				if(vp.therapyKnowledge.get(therapyId).isSufficientlyInformed()) {					
//					numberOfTherapiesToChooseFrom += 1;
//				}
//			}
//		}
//		
//		return (numberOfTherapiesToChooseFrom >= vp.MINIMUM_AMOUNT_OF_THERAPIES_TO_MAKE_A_DECISION);
//	}
//
//	// Return what the current choice would be based on a utility function
//	//		Return 0 if the VP needs more information
//	//		Return 1, 2, 3, 4 which represents the therapyId that has the highest preference
//	private int getCurrentChoice() {
//		System.out.println("What should we choose?");
//		int preferredTherapy = 0;
//		double highestImpact = -Double.MAX_VALUE;
//		
//		if(!choicePossible()) {
//			return preferredTherapy;
//		} else {
//			for (Integer therapyId : vp.importantTherapies.keySet()) {
//				TherapyKnowledge t = vp.therapyKnowledge.get(therapyId);
//				ImportantTherapy it = vp.importantTherapies.get(therapyId);
//				
//				if(it.isRelevant() && it.nameKnown() && t != null) {
//					double impact = t.getTherapyImpact();
//	
//					if(impact > highestImpact) {
//						preferredTherapy = therapyId;
//					}
//	
//					System.out.println(impact);
//					System.out.println("preferred: " + preferredTherapy);
//					}
//				}
//	
//			return preferredTherapy;
//		}
//	}
}