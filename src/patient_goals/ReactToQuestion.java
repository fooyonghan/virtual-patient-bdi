package patient_goals;

import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalCreationCondition;
import vp_bdiv3.VirtualPatientBDI;

@Goal
public class ReactToQuestion {
	@GoalCreationCondition(beliefs="questionReceived")
	private static ReactToQuestion createGoal(VirtualPatientBDI vp)
	{
		return new ReactToQuestion();
	}
}
