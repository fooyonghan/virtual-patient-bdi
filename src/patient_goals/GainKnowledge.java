package patient_goals;

import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalCreationCondition;
import vp_bdiv3.VirtualPatientBDI;

@Goal
public class GainKnowledge {
	@GoalCreationCondition(beliefs="isAwkward")
	private static GainKnowledge createGoal(VirtualPatientBDI vp)
	{
		// Take into account thearpyOnMind
		// Check if name is known of therapy	
		return new GainKnowledge();
		// Maybe a check for time, i.e. patient feels awkward
	}
}
