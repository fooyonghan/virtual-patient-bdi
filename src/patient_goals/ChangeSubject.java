package patient_goals;

import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalCreationCondition;
import vp_bdiv3.VirtualPatientBDI;

@Goal
public class ChangeSubject {
	@GoalCreationCondition(beliefs="conversationKnowledge")
	private static ChangeSubject createGoal(VirtualPatientBDI vp)
	{
		return new ChangeSubject();
	}
}
